
/**
 * Remove last val consecutive values, starting from the end of the array.
 * @param {*[]} array Will be modified. The remaining array will never be empty
 * @param {*} val 
 * @returns removed elements
 */
function trimLastValues(array, val) {
    let i = array.length - 1;
    while (i >= 1 && array[i] === val) { i--; }
    return array.splice(i + 1);
}

function isNullOrUndefined(val) {
    return val === undefined || val === null;
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

/**
 * 
 * @param {any} value 
 * @param {string} expectedTypeOf 
 * @param {string} paramName 
 * @param {boolean} optionalParam 
 */
function throwIfNotExpectedTypeOf(value, expectedTypeOf, paramName, optionalParam = false)
{
    if(typeof value !== expectedTypeOf)
        throw new Error(paramName + " must be a " + expectedTypeOf + " value." + (optionalParam ? " It's optional." : "") + " Found " + value + "(" + typeof value + ")");
}

/*
 * @returns {number} Il numero quadrato positivo subito più grande del valore passato
 * @param {number} num Un numero intero
 *
function roundToSquareNumber(num) {
    if (!Number.isInteger(num))
        throw new Error("num must be an integer");

    let i = 1;
    let res = 1;
    while (res < num) { i++; res = i * i; }
    return res;
}
*/