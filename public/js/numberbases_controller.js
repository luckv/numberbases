

class PositiveDecomposedNumberGraphicRepresentation {

    /**
     * @param {Base10Converter} base10Converter A base 10 converter for a specific radix
     */
    constructor(base10Converter) {

        if (isNullOrUndefined(PositiveDecomposedNumberGraphicRepresentation._baseValue_jQueryObject))
            PositiveDecomposedNumberGraphicRepresentation._baseValue_jQueryObject = $(PositiveDecomposedNumberGraphicRepresentation._baseValue_cssSelector);

        if (isNullOrUndefined(PositiveDecomposedNumberGraphicRepresentation._numberValue_jQueryObject))
            PositiveDecomposedNumberGraphicRepresentation._numberValue_jQueryObject = $(PositiveDecomposedNumberGraphicRepresentation._numberValue_cssSelector);

        if (isNullOrUndefined(PositiveDecomposedNumberGraphicRepresentation._decompositionString_jQueryObject))
            PositiveDecomposedNumberGraphicRepresentation._decompositionString_jQueryObject = $(PositiveDecomposedNumberGraphicRepresentation._decompositionString_cssSelector);

        if (!base10Converter instanceof Base10Converter)
            throw new Error("base10Converter must be an instance of Base10Converter");

        this._base10Converter = base10Converter;
        let radix = base10Converter.digitConverter.radix;
        this._modelDecomposition = new PositiveDecomposedNumber(radix);
        this._viewDecomposition = new GraphicDecomposition(throwIfNotFoundJQuerySelector("#digits_container"), radix);

        PositiveDecomposedNumberGraphicRepresentation._baseValue_jQueryObject.text(''.concat(radix));
        this._updateGraphicRepresentation();
    }

    get radix() {
        return this._base10Converter.digitConverter.radix;
    }

    get base10Converter() {
        return this._base10Converter;
    }

    get base10_value() {
        return this._modelDecomposition.base10_value;
    }

    set base10_value(num) {
        this._modelDecomposition.base10_value = num;
        this._updateGraphicRepresentation();
    }

    shrink_graphicDecomposition()
    {
        this._updateGraphicRepresentation(true);
    }

    increment() {
        this._modelDecomposition.increment();
        this._updateGraphicRepresentation();
    }

    decrement()
    {
        this._modelDecomposition.decrement();
        this._updateGraphicRepresentation();
    }

    algebricAdd(num) {
        this._modelDecomposition.algebricAdd(num);
        this._updateGraphicRepresentation();
    }

    /**
     * Change the radix used. If preserveBase10Value is true, the old base 10 value will be preserved. 
     * If preserveDecomposition is true, decomposition visualized will remain unchanged. preserveBase10Value and preserveDecomposition can't be both true.
     * 
     * @param {Base10Converter} base10Converter A base 10 converter for a specific radix
     * @param {boolean} preserveBase10Value Indicates if the old base 10 value represented needs to be preserved. Default is false.
     *                                      If true preserveDecomposition must be false
     * @param {boolean} preserveDecomposition Indicates if the decomposition visualized should remain unchanged. Default is false. 
     *                                        If true must be radix >= this.radix and preserveBase10Value must be false
     */
    changeRadix(base10Converter, preserveBase10Value = false, preserveDecomposition = false) {

        if (typeof preserveBase10Value != 'boolean' || typeof preserveDecomposition != 'boolean')
            throw new Error("preserveBase10Value and preserveDecomposition must be boolean. Found preserveBase10Value:" + typeof preserveBase10Value + ", preserveDecomposition:" + typeof preserveDecomposition);

        if (preserveBase10Value && preserveDecomposition)
            throw new Error("preserveBase10Value and preserveDecomposition can't be both true");

        if (!base10Converter instanceof Base10Converter)
            throw new Error("base10Converter must be an instance of Base10Converter");

        let new_radix = base10Converter.digitConverter.radix;

        if (preserveDecomposition && new_radix < this.radix)
            throw new Error("Can't preserve decomposition representation if you try to use a smaller radix. base10Converter.radix must be >= " + this.radix);

        this._base10Converter = base10Converter;

        let oldValue = undefined;

        if (preserveBase10Value)
            oldValue = this._modelDecomposition.base10_value;

        if (preserveDecomposition) {
            this._modelDecomposition.changeRadix(new_radix);
        }
        else {
            delete this._modelDecomposition;
            this._modelDecomposition = new PositiveDecomposedNumber(new_radix);
        }

        if (preserveBase10Value)
            this._modelDecomposition.base10_value = oldValue;

        PositiveDecomposedNumberGraphicRepresentation._baseValue_jQueryObject.text(''.concat(new_radix));
        this._viewDecomposition.changeRadix(new_radix);

        this._updateGraphicRepresentation( preserveBase10Value || !preserveDecomposition );  //Riduco la dimensione della rappresentazione grafica, se necessario
    }

    /**
     * Update the graphic representation of the number in the specified radix. Update all other graphic content related to the value and the radix, like labels, numbers, ecc.
     * @param {boolean} shrink If true tell to use the minimun digitsContainer necessary
     */
    _updateGraphicRepresentation(shrink = false) {

        let base10value = this._modelDecomposition.base10_value;
        let decomposition_num = this._modelDecomposition.decomposition;
        let decomposition_values = this._base10Converter.getRadixDecomposition(decomposition_num);
        let representation_string = decomposition_values.reduceRight((a, b) => a.concat(b));

        if(shrink && decomposition_num.length < this._viewDecomposition.digitsCount)
            this._viewDecomposition.reduceNumberOfDigits(decomposition_num.length);

        //Aggiorno la rappresentazione grafica della decomposizione
        this._viewDecomposition.setValues(decomposition_num, decomposition_values, this._base10Converter.digitConverter.getConvertedDigit(0));

        //Aggiorno il valore del numero rappresentato
        PositiveDecomposedNumberGraphicRepresentation._numberValue_jQueryObject.html(representation_string.concat(new String(this.radix).sub()));

        
        //---------------------------------------------------------------------------------------------------------
        //---Aggiorno la stringa che descrive le operazioni per calcolare il numero

        //Aggiungo le cifre mancanti per creare un elemento della decomposizione per ogni cifra rappresentata da un digit container
        let digitsMissing = this._viewDecomposition.digitsCount - decomposition_num.length;
        if (digitsMissing > 0)
            decomposition_num = decomposition_num.concat(Array(digitsMissing).fill(0));


        //Sostituisce ogni cifra decomposta con la formula matematica che la rappresenta, cioè value * radix^index                             
        let decomposition_operations = decomposition_num
            .map((value, index) => { return value + '&#215;' + this.radix + new String(index).sup(); }, this)
            //e concateno tutti i valori così da creare una somma
            .reduceRight((a, b) => a.concat(' + ', b));

        //Aggiungo al termine della somma il risultato in base 10
        decomposition_operations = decomposition_operations.concat(' = ', base10value, new String(10).sub());

        PositiveDecomposedNumberGraphicRepresentation._decompositionString_jQueryObject.html(decomposition_operations);
    }
}
PositiveDecomposedNumberGraphicRepresentation._baseValue_cssSelector = '.base_value';
PositiveDecomposedNumberGraphicRepresentation._numberValue_cssSelector = '.number_value';
PositiveDecomposedNumberGraphicRepresentation._decompositionString_cssSelector = '.decomposition_string';


class ButtonController {

    /**
     * 
     * @param {PositiveDecomposedNumberGraphicRepresentation} decomposedNumberGraphicRepresentation The object for manipulating the graphic representation of a decomposed number
     */
    constructor(decomposedNumberGraphicRepresentation) {

        if (!decomposedNumberGraphicRepresentation instanceof PositiveDecomposedNumberGraphicRepresentation)
            throw new Error("decomposedNumberGraphicRepresentation must an instance of PositiveDecomposedNumberGraphicRepresentation");

        this._graphicDecomposedNumber = decomposedNumberGraphicRepresentation;

        this._inputBox = new InputBox( throwIfNotFoundJQuerySelector('#input_number') , (str) => decomposedNumberGraphicRepresentation.base10Converter.isAValidRadixDecomposition( str.split('')) );
        this._inputBox_errorMessagejQuery = throwIfNotFoundJQuerySelector('#input_number_errorMessage');

        //Creo l'array con le etichette dei pulsanti per il dialpad
        let dialPad_labels = Array(this._graphicDecomposedNumber.radix).fill(undefined).map((value, index) => this._graphicDecomposedNumber.base10Converter.digitConverter.getConvertedDigit(index), this);

        this._dialPad = new DialPad(throwIfNotFoundJQuerySelector(ButtonController._dialPad_cssSelector), dialPad_labels);

        //Ogni volta che premo un pulsante sul dialpad aggiungo il rispettivo valore al numero che sto visualizzando
        this._dialPad.addDialOnClickCallback(((btn_index, label) => {
            this._graphicDecomposedNumber.algebricAdd(btn_index);
        }).bind(this));

        $(ButtonController._incrementButton_cssSelector).click(((eventObject) => { this._graphicDecomposedNumber.increment() }).bind(this));

        $('#button_set').click( (() => { 
            if(this._checkIfValidInputBoxValue())
            {
                let value = this._graphicDecomposedNumber.base10Converter.getBase10Value(this._inputBox.value.split('').reverse());
                this._graphicDecomposedNumber.base10_value = value;
            }
        }).bind(this) );
        
        $('#button_add').click( (() => { 
            if(this._checkIfValidInputBoxValue()) 
            {
                let value = this._graphicDecomposedNumber.base10Converter.getBase10Value(this._inputBox.value.split('').reverse());
                this._graphicDecomposedNumber.algebricAdd(value);
            }
        }).bind(this) );

    }

    _checkIfValidInputBoxValue()
    {
        if(this._inputBox.hasAValidInput)
        {
            this._inputBox_errorMessagejQuery.hide();
            return true;
        }
        else
        {
            this._inputBox_errorMessagejQuery.show();
            return false;
        }
    }

    get graphicDecomposedNumber() {
        return this._graphicDecomposedNumber;
    }

    get dialPad() {
        return this._dialPad;
    }

}
ButtonController._incrementButton_cssSelector = '.increment-button';
ButtonController._dialPad_cssSelector = "#dialpad";



