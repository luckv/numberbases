
/**
 * Returns true if obj is a non null object with an integer property length != 0
 * @returns {boolean}
 * @param {*} obj 
 */
function isAValidJQueryObject(obj) {
    return obj !== null && obj !== undefined && obj.length !== 0;
}

function throwIfNotValidjQueryObject(obj, param_name) {
    if (!isAValidJQueryObject(obj))
        throw new Error(param_name + " is not a valid jQuery object (" + typeof obj + ")");
}

function throwIfNotFoundJQuerySelector(selector) {
    let obj = $(selector);
    if (!isAValidJQueryObject(obj))
        throw new Error("Doesn't exists any element with selector '" + selector + "' (" + obj + ")");

    return obj;
}

/**
 * Create a JQuery element without attaching it to the DOM tree.
 * @param {string} elementType The element type (p, button, div, ...)
 * @param {string} classes The classed to be added to this element separated by spaces
 * @param {string} id The id of this element
 */
function createJQueryElement(elementType, classes = '', id = '') {

    if (isNullOrUndefined(elementType) || ''.concat(elementType) === '')
        throw new Error("elementType must be a value convertible to a non empty string. Found'" + elementType + "'(" + typeof elementType + ")");

    if (isNullOrUndefined(classes))
        throw new Error("classes must be a non null and defined value. Found'" + classes + "'. Parameter is optional");

    if (isNullOrUndefined(id))
        throw new Error("id must be a non null and defined value. Found'" + id + "'. Parameter is optional");

    if (id != '') {
        let alreadyExists = document.getElementById(id);
        if (alreadyExists !== null)
            throw new Error("An element with id '" + id + "' already exists. Only one element with a given id can exist at a time");
    }


    let elem = $(document.createElement(elementType));
    elem.addClass(classes);

    if (id != '')
        elem[0].id = id;

    return elem;
}


class GraphicDigitRepresentation {

    /**
     * Creates an instance of DigitContainer, 
     * that can contain at least "placeholder_count" digitplaceholders
     * 
     * @constructor
     * @author: Luca Visentin
     * @this {DigitContainer}
     * @param {jQuery} jQueryElement A jQuery element, possibly a div
     * @param {number} placeholder_count The value of the base number of this digit
     * @param {boolean} square Tell if width and height of this digitContainer must be equal. Default let rectangular shape, to use minimum horizontal space
     */
    constructor(jQueryElement, placeholder_count, square = false) {
        throwIfNotValidjQueryObject(jQueryElement, "jQueryElement");

        if (!Number.isInteger(placeholder_count) || placeholder_count < 1)
            throw new Error("placeholder_count must be an integer >= 1. Found '" + placeholder_count + "'");

        if (typeof square != 'boolean')
            throw new Error("square must be a boolean. Can be omitted, and its default value is false");

        this._jQuery = jQueryElement;

        if (isNullOrUndefined(GraphicDigitRepresentation._placeholderjQueryBase))
            GraphicDigitRepresentation._placeholderjQueryBase = createJQueryElement('div', 'rounded-circle digit_placeholder');

        //Elimino i figli del digitContainer
        this._jQuery.empty();
        //Inizializzo la lista dei placeholders a vuota
        this._placeholders = [];

        //Imposto il numero di placeholders
        this.changeNumberOfPlaceholders(placeholder_count, square);
    }

    /**
     * Change the number of placeholders inside the digit container, adjusting its size if necessary.
     * Leave the state of the remaining digit placeholders unchanged
     * @param {number} new_placeholder_count 
     * @param {boolean} square Tell if width and height of this digitContainer should be equal. Default let rectangular shape
     */
    changeNumberOfPlaceholders(new_placeholder_count, square = false) {
        if (!Number.isInteger(new_placeholder_count) || new_placeholder_count < 1)
            throw new Error("new_placeholder_count must be an integer >= 1. Found '" + new_placeholder_count + "'");

        if (!Array.isArray(this._placeholders))
            throw new Error("this.placeholders must be an array, even empty if necessary");

        if (new_placeholder_count != this._placeholders.length) {
            //Ricalcolo altezza e larghezza di digitContainer
            //Cerco di mantenere le proporzioni tra altezza e larghezza vicino a 1
            //Se il numero di cifre non riempie un quadrato, allora diminuisco la larghezza in modo da ridurre lo spazio occupato 

            //30px: dimensione di un digitplaceholder
            const placeholder_size = 30;
            //Numero di digitplaceholder che possono stare al massimo per ogni colonna di questo digitcontainer
            const placeholder_percolumn = Math.ceil(Math.sqrt(new_placeholder_count));
            //Dimensioni del contenitore se avesse la forma di un quadrato
            //const digitContainerSquareSize = placeholder_percolumn * placeholder_size;

            //this.jQuery.height(digitContainerSquareSize);

            if (square)
                this._jQuery.width(placeholder_percolumn * placeholder_size);
            //this.jQuery.width(digitContainerSquareSize);
            else {
                //Calcolo il numero di colonne necessarie a contenere tutti i placeholder_count elementi
                //in questo modo, risparmio spazio in larghezza, per mettere più digitcontainers
                this._jQuery.width(Math.ceil(new_placeholder_count / placeholder_percolumn) * placeholder_size);
            }

            let i = 0;

            if (new_placeholder_count > this._placeholders.length) {
                //Creo un array degli elementi mancanti da aggiungere..
                let placeholders_adding = new Array(new_placeholder_count - this._placeholders.length);
                let old_placeholders = this._placeholders;
                //..e lo popolo. Uso un for per rispettare l'ordine di inserimento degli elementi html
                for (i = 0; i < placeholders_adding.length; i++)
                    placeholders_adding[i] = GraphicDigitRepresentation._placeholderjQueryBase.clone().appendTo(this._jQuery);
                //placeholders_adding[i] = new DigitPlaceholder(this);

                this._placeholders = old_placeholders.concat(placeholders_adding);
            }
            else {
                //Rimuovo gli elementi in più dall'array e dal DOM html
                delete this._placeholders.splice(new_placeholder_count, this._placeholders.length).forEach((value) => { value.remove(); });
            }
        }

    }


    /**
     * Set all placeholders unfilled
     */
    unfillAll() {
        this._placeholders.forEach((value) => value.removeClass(GraphicDigitRepresentation._placeholderfilled_cssClassName));
    }

    /**
     * Set all first 'digit_value' digitplaceholders filled, the others unfilled.
     * @param {number} digit_value Must be 0 <= digit_value <= this.placeholders.length
     * @throws {Error} If digit_value is not an integer value and it's not in the interval specified
     */
    fillUntil(digit_value) {
        if (!Number.isInteger(digit_value) || digit_value < 0 || digit_value > this._placeholders.length)
            throw new Error("digit_value must be an integer and 0 <= digit_value <= " + (this._placeholders.length) +
                ". Found '" + digit_value + "'");

        this._placeholders.forEach((value, index) => {
            if (index < digit_value)
                value.addClass(GraphicDigitRepresentation._placeholderfilled_cssClassName);
            else
                value.removeClass(GraphicDigitRepresentation._placeholderfilled_cssClassName);
        });

    }

    /*
    _setFill(index, filledState = true)
    {
        if (filledState)
            this._placeholders[index].addClass(DigitContainer._placeholderfilled_cssClassName);
        else
            this._placeholders[index].removeClass(DigitContainer._placeholderfilled_cssClassName);
    }
    */

}
GraphicDigitRepresentation._placeholderjQueryBase = null;
GraphicDigitRepresentation._placeholderfilled_cssClassName = 'filled';

class GraphicDecomposition {

    constructor(jQueryElement, radix) {
        throwIfNotValidjQueryObject(jQueryElement, "jQueryElement");
        this._containerjQuery = jQueryElement;

        this.changeRadix(radix);
        this.reset();
    }

    /**
     * Returns the number of digits on the screen
     */
    get digitsCount() {
        return this._digitContainersObj.length;
    }


    /**
     * Reset the state of the graphic representation.
     * Removes all the digits containers and leave an empty one. The radix remains unchanged
     */
    reset() {
        this._containerjQuery.empty();
        delete this._lastDigit;
        this._digitValuesjQuery = [];
        this._digitContainersObj = [];
        this._appendDigit();
    }

    /**
     * Change the radix for the decomposition. Leave all the actual digits containers, and theirs content, but change the number of placeholders
     * @param {number} radix The new radix for the decomposition
     */
    changeRadix(radix) {
        if (!Number.isInteger(radix) || radix < 2)
            throw new Error("radix must be an integer >= 2. Found '" + radix + "'");

        if (isNullOrUndefined(this._placeholders_per_digit) || this._placeholders_per_digit != radix - 1) {
            this._placeholders_per_digit = radix - 1;
            if (this._digitContainersObj !== undefined)
                this._digitContainersObj.forEach((value) => { value.changeNumberOfPlaceholders(this._placeholders_per_digit); }, this);
        }

    }

    reduceNumberOfDigits(targetDigitsCount) {
        if (!Number.isInteger(targetDigitsCount) || targetDigitsCount < 1 || targetDigitsCount > this.digitsCount)
            throw new Error("targetDigitsCount must be an integer between 1 and " + this.digitsCount + ". Found " + targetDigitsCount);

        let digitsRep = this._containerjQuery.children().toArray();  //Ottengo tutti i figli diretti, div creati da _appendDigit() che contengono la rappresentazione di una cifra


        let childToRemove = digitsRep.slice(targetDigitsCount);
        childToRemove.forEach((elem) => { elem.remove(); });
        this._digitContainersObj.splice(targetDigitsCount);
        this._digitValuesjQuery.splice(targetDigitsCount);


        this._lastDigit = $(digitsRep[targetDigitsCount - 1]);
        this._removeArrow(this._lastDigit);
    }



    /**
     * Set the number of placeholders filled in every GraphicDigit by looking at the decomposition array. 
     * Set the label above every graphic digit by looking at the value_labels array. 
     * value_labels and decomposition must have the same length
     * If necessary, the number of digit containers displayed will grow to show all the data in the parameters.
     * @param {number[]} decomposition An array of integers.
     * @param {string[]} value_labels An array of values, possibly strings.
     * @param {string} label_0 A string label for value 0 when the decomposition array is shorter than the decomposition displayed
     */
    setValues(decomposition, value_labels, label_0) {
        if (!Array.isArray(decomposition) || !Array.isArray(value_labels) || decomposition.length !== value_labels.length)
            throw new Error("decomposition and value_labels must be two arrays and have the same length");

        if (isNullOrUndefined(label_0))
            throw new Error("label_0 must not be null or undefined. Found " + label_0);

        let i = 0;
        const minimum_displayable = Math.min(decomposition.length, this._digitContainersObj.length);
        const digits_missing = decomposition.length - this._digitContainersObj.length;

        for (i = 0; i < minimum_displayable; i++)
            this._setSingleValue(i, decomposition[i], value_labels[i]);


        if (digits_missing > 0) {
            //Aggiungo i digit container mancanti   
            let arr_index;
            for (i = 1, arr_index = minimum_displayable; i <= digits_missing; i++ , arr_index++) {
                this._appendDigit();

                //Scrivo il valore nel digit container appena aggiunto
                this._setSingleValue(arr_index, decomposition[arr_index], value_labels[arr_index]);
            }
        }
        else {
            //Metto nei digit container mancanti il valore 0
            while (i < this._digitContainersObj.length) {
                this._setSingleValue(i, 0, label_0);
                i++;
            }
        }

    }

    _setSingleValue(position, value, label) {
        this._digitValuesjQuery[position].text(label);
        if (value !== 0)
            this._digitContainersObj[position].fillUntil(value);
        else
            this._digitContainersObj[position].unfillAll();
    }

    _appendDigit() {
        let digit = createJQueryElement('div', 'd-flex flex-column flex-md-row-reverse');
        this._containerjQuery.append(digit);

        this._appendDigitContainer(digit);

        //Se ho più di una cifra, aggiungo la freccia che punta alla cifra appena creata
        if (this.digitsCount > 1)
            this._appendArrow(this._lastDigit);

        this._lastDigit = digit;  //Sovrascrive il valore dell'ultima cifra nella rappresentazione, se sto inserendo la prima cifra, sarà undefined o null il vecchio valore
    }

    _appendArrow(parent_jQuery) {
        if (isNullOrUndefined(GraphicDecomposition._elementArrowJQueryBase)) {
            let arrow1 = createJQueryElement('span', "oi align-self-center");
            let arrow2 = arrow1.clone();

            arrow1.addClass("oi-chevron-bottom py-3 hidden-md-up");  //Freccia verso il basso fino a md (< 768px)
            arrow2.addClass("oi-chevron-left px-3 hidden-sm-down");  //Freccia a sinistra da md in poi (>= 768px)

            GraphicDecomposition._elementArrowJQueryBase = [arrow1, arrow2];
        }

        parent_jQuery.append(GraphicDecomposition._elementArrowJQueryBase[0].clone(), GraphicDecomposition._elementArrowJQueryBase[1].clone());
    }

    _removeArrow(parent_jQuery) {
        parent_jQuery.find('.oi').remove();
    }

    _appendDigitContainer(parent_jQuery) {
        if (isNullOrUndefined(GraphicDecomposition._elementDigitContainerJQueryBase)) {
            GraphicDecomposition._elementDigitContainerJQueryBase = createJQueryElement('div', 'd-flex flex-row flex-md-column align-items-center justify-content-between m-0 p-0');

            GraphicDecomposition._elementDigitContainerJQueryBase.append(createJQueryElement('p', 'm-0 p-1 digit-value_text ' + GraphicDecomposition._digitValue_cssClassName));
            GraphicDecomposition._elementDigitContainerJQueryBase.append(createJQueryElement('div', 'd-flex flex-row flex-wrap rounded ' + GraphicDecomposition._digitContainer_cssClassName));
            GraphicDecomposition._elementDigitContainerJQueryBase.append(createJQueryElement('p', 'm-0 p-1 power-base_text'));
        }

        let cont = GraphicDecomposition._elementDigitContainerJQueryBase.clone();

        let digitValuejQuery = cont.find('.' + GraphicDecomposition._digitValue_cssClassName);
        cont.find('.power-base_text').text(this._digitContainersObj.length);
        parent_jQuery.append(cont);

        this._digitValuesjQuery.push(digitValuejQuery);
        this._digitContainersObj.push(new GraphicDigitRepresentation(cont.find('.' + GraphicDecomposition._digitContainer_cssClassName), this._placeholders_per_digit));
    }

}
GraphicDecomposition._digitValue_cssClassName = 'digit-value';
GraphicDecomposition._digitContainer_cssClassName = 'digit_container';
GraphicDecomposition._elementArrowJQueryBase = null;
GraphicDecomposition._elementDigitContainerJQueryBase = null;

class DialPad {

    /**
    * @constructor
    * @author: Luca Visentin
    * @this {DialPad}
    * @param {object} jQueryElement The jQuery object that contains the dialpad
    * Instanstiate a DialPad object for displaying a dialpad on the screen, with numbers starting from 0
    * @param {string[]} labels Array of labels associated with values of every button in the dialpad. Button for value i has label of index i in labels. Default value is an array with only one element, '0'
    */
    constructor(jQueryElement, labels = ['']) {
        throwIfNotValidjQueryObject(jQueryElement, "jQueryElement");

        if (isNullOrUndefined(DialPad._elementjQueryBase))
            DialPad._elementjQueryBase = createJQueryElement('button', 'btn btn-outline-secondary btn-sm');


        this._jQuery = jQueryElement;
        this._dials = [];
        this._dialOnClickCallbacks = [];

        this.changeDials(labels);
    }

    /**
     * 
     * @param {function(number, string) => void} callback A callback that receives as first argument the integer position of the button that has been pressed, as second argument the displayed value
     */
    addDialOnClickCallback(callback) {
        this._dialOnClickCallbacks.push(callback);
    }

    /**
     * 
     * @param {number} btn_index 
     * @param {string} label 
     */
    _notifyOnClick(btn_index, label) {
        this._dialOnClickCallbacks.forEach(async (callback) => {
            callback(btn_index, label);
        });
    }

    /**
     * Change the numbers of dials displayed, and his content, according to the array labels passed.
     * Only the strict needed elements will be recreated or removed
     * @param {string[]} labels Array of labels associated with values of every dial in the dialpad. Button for value i will have label of index i in labels
     */
    changeDials(labels) {
        if (!Array.isArray(labels))
            throw new Error("labels must be an array. Possibly of strings");

        //Ricalcolo altezza e larghezza di digitContainer
        //Cerco di mantenere le proporzioni tra altezza e larghezza vicino a 1
        //Se il numero di cifre non riempie un quadrato, allora diminuisco la larghezza in modo da ridurre lo spazio occupato 

        //42px: dimensione di un pulsante in dial pad
        const dialbutton_size = 42;

        //Se ho più di 25 pulsanti inverto lo stile della direzione in cui disporre gli elementi
        if (labels.length >= 25)
            this._jQuery.addClass("flex-row").removeClass("flex-row-reverse");
        else
            this._jQuery.addClass("flex-row-reverse").removeClass("flex-row");

        let i = 0;
        //Se devo inserire nuove etichette
        if (labels.length > this._dials.length) {

            //Sovrascrivo le etichette dei pulsanti già presenti
            for (i = 0; i < this._dials.length; i++)
                this._dials[i].text(labels[i]);

            //Creo un array degli elementi mancanti da aggiungere..
            let dials_adding = new Array(labels.length - this._dials.length);
            let old_dials = this._dials;

            //..e lo popolo. Uso un for per rispettare l'ordine di inserimento degli elementi html
            for (i = 0; i < dials_adding.length; i++) {
                dials_adding[i] = DialPad._elementjQueryBase.clone().text(labels[this._dials.length + i]).prependTo(this._jQuery);
                //Aggiungo un listener agli eventi di pressione del pulsante pulsante

                dials_adding[i].click(this._dials.length + i, (eventObject) => { this._notifyOnClick(eventObject.data, eventObject.delegateTarget.textContent); });
            }

            //Unisco i due array
            this._dials = old_dials.concat(dials_adding);
        }
        else {
            //Se devo rimuovere delle etichette
            //Sovrascrivo le etichette dei pulsanti già presenti
            for (i = 0; i < labels.length; i++)
                this._dials[i].text(labels[i]);

            //Elimino le cifre in più rimuovendole dall'array e dal DOM html
            delete this._dials.splice(labels.length, this._dials.length).forEach((value) => { value.remove() });
        }
    }

}
DialPad._elementjQueryBase = null;


class InputBox {

    /**
     * 
     * @param {jQuery} textBox_jQuery 
     * @param {function(string) => boolean} inputVerifier 
     */
    constructor(textBox_jQuery, inputVerifier) {
        throwIfNotValidjQueryObject(textBox_jQuery, 'textBox_jQuery');

        this._textBox_jQuery = textBox_jQuery;
        this._value = '';
        this._isValid = true;

        textBox_jQuery.on('input', null, this, function (event) {

            let textbox = $(this);
            let inputBox_obj = event.data;
            inputBox_obj._value = ''.concat(textbox.val());
            inputBox_obj._isValid = inputVerifier(inputBox_obj._value);

            if (inputBox_obj._isValid)
                textbox.removeClass('text-danger').addClass('text-success');
            else
                textbox.removeClass('text-success').addClass('text-danger');

        });

        this.changeInputVerifier(inputVerifier);
    }

    get inputVerifier() {
        return this._inputVerifier;
    }

    changeInputVerifier(inputVerifier) {
        if (!$.isFunction(inputVerifier))
            throw new Error("inputVerifier is required to be a function(string) => boolean");

        this._inputVerifier = inputVerifier;
        this._textBox_jQuery.trigger('input');
    }

    /**
     * @returns {string}
     */
    get value() {
        return this._value;
    }

    /**
     * @returns {boolean}
     */
    get hasAValidInput() {
        return this._isValid;
    }
}