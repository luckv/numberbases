

/**
 * Test the callback digitToString for all values between start and end (included). 
 * The execution of this function without exception ensure that any unexpected exception will be thrown
 * by this callback and all its executions return usable values, not null and not empty strings. 
 * @param {function(number) => string} digitToString Callback to be tested for values between start and end
 * @param {number} start start <= end
 * @param {number} end  end >= start
 * @param {string} callbackName The callback name to be reported when trowing exception
 * @throws {Error} If there is any value between start and end that throws exception or returns unusable values, like empty strings or null
 */
function testDigitToStringCallback(digitToString, start, end, callbackName) {
    try {
        for (let i = start; i <= end; i++) {

            let res = digitToString(i);
            if (res == null || res == undefined || new String(res).toString().trim() === "")
                throw new Error(callbackName + "(" + i + "): " + res + " (" + typeof res + ")");
        }
    }
    catch (e) {
        throw new Error(callbackName + " callback passed isn't good for values between " + start + " and " + end + ". Cause: " + e.message);
    }
}

/**
 * Convert a primitive javascript type to a BigInteger object, if possible. Any BigInteger object will be returned as it is.
 * @param {any} num Any value convertibile to a BigInteger object
 * @returns {BigInteger} A BigInteger object representing the number passed
 * @throws {Error} If it's not possible to convert the value to a BigInteger object
 */
function tryConvertToBigInteger(num)
{
    if(num instanceof BigInteger)
        return num;
    else{
        try{
            return BigInteger(num);
        }
        catch(e)
        {
            throw new Error("Can't convert value '" + num + "' to a BigInteger object. Cause: " + e.message);
        }
    }
}

/**
 * Throw an exception if radix is not a safe integer for javascript or is not >= 2. If is a valid radix, does nothing.
 * If radix is not a safe integer, division and remainder operation will not be correct.
 * @param {number} radix An integer, guess to be a radix value
 * @throws {Error} If radix is not a safe integer for javascript or is < 2
 */
function throwIfNotValidRadix(radix, min_val = 1)
{
    if (!Number.isSafeInteger(radix) || radix < min_val)
        throw new Error("radix must be an integer >= " + min_val + ", and must be a safe integer for javascript, so <= " + Number.MAX_SAFE_INTEGER + ". Found " + radix);
}

class PositiveDecomposedNumber {
    /**
     * Create a number of base {@argument radix} with value 0
     * @param {number} radix base wich is used to decompose the number. Must be >= 2
     */
    constructor(radix) {
        throwIfNotValidRadix(radix, 2);

        this._radix = radix;
        this._decomposition = [0];
        this._big_base10Value = BigInteger.ZERO;
    }

    get radix() {
        return this._radix;
    }

    /**
     * The decomposition of this number. Every element is an integer between 0 and this.radix - 1. The array is never empty
     * @returns {number[]}
     */
    get decomposition() {
        let res = this._decomposition.slice();
        trimLastValues(res, 0);
        return res;
    }

    /**
     * Change the radix of this number but leave the decomposition unchanged.
     * For example if we have radix 4 with decomposition [2,3,1] and call changeRadix(5), the decomposition will remain [2,3,1]. 
     * The base10_value will change accordingly
     * @param {number} radix An integer >= this.radix
     * @throws {Error} If radix don't respect parameters requested
     */
    changeRadix(radix) {
        throwIfNotValidRadix(radix, 2);  //Control first that radix is a valid radix value
        if (radix < this._radix)  //Then check that it is at least bigger than the older radix was
            throw new Error("radix must be an integer >= " + this._radix + ". Found '" + radix);

        this._radix = radix;
    }

    /**
     * Increment by 1 unit the value of this number.
     */
    increment() { 
        this._big_base10Value = this._big_base10Value.next();

        this._decomposition[0]++;
        let i=1;
        while(this._decomposition[i-1] >= this._radix && i < this._decomposition.length)
        {
            this._decomposition[i-1] = 0;
            this._decomposition[i]++;
            i++;
        }

        //Se ho finito l'array, allora vado a accodare una cifra di un'unità alla decomposizione
        if(this._decomposition[i-1] >= this._radix)
        {
            this._decomposition[i-1] = 0;
            this._decomposition.push(1);
        }
    }

    /**
     * Decrement by 1 unit the value of this number, if possible. Otherwise it generates an exception
     */
    decrement() {
        if (this._big_base10Value.isZero())
            throw new Error("Can't decrement if its value is zero");

        this._big_base10Value = this._big_base10Value.prev();

        this._decomposition[0]--;
        let i = 1;
        while (this._decomposition[i - 1] < 0) {
            this._decomposition[i - 1] = this._radix - 1;
            this._decomposition[i]--;
            i++;
        }

    }

    /**
     * Sum the decimal number passed to the number that this object represents. Calculate and apply carry if necessary
     * The resulting number can't be negative
     * @param {any} num A BigInteger object or a convertibile value. Must be num >= -1*this.base10_value
     */
    algebricAdd(num) {
        var big_dec_num = tryConvertToBigInteger(num);
        {
            let actualDecValueInverseDecValue = this._big_base10Value.negate();
            if (big_dec_num.compare(actualDecValueInverseDecValue) == -1)
                throw new Error("num must be an integer >= " + actualDecValueInverseDecValue.toString() + ". Found '" + big_dec_num.toString() + "'");
        }

        big_dec_num = big_dec_num.add(this._big_base10Value);  //Sommo il valore attuale a quello da sommare, così da ottenere il nuovo valore dell'oggetto
        this._internal_setBase10Value(big_dec_num);
    }


    /**
     * Set the decimal number that this object represents
     * @param {any} num A BigInteger object or a convertibile value. Must be num >= 0
     */
    set base10_value(num) {
        var big_dec_num = tryConvertToBigInteger(num);

        if(big_dec_num.sign() < 0)
            throw new Error(arg_name + " must be an integer >= 0. Found '" + big_dec_num.toString() + "'");

        this._internal_setBase10Value(big_dec_num);
    }


    /**
     * @returns {number} The decimal value of the number that this object represents
     */
    get base10_value() {
        return this._big_base10Value;
    }

    _internal_setBase10Value(big_dec_num) {
        if(big_dec_num.sign() < 0)
            throw new Error("INTERNAL ERROR: big_dec_num mustn't be < 0");

        if (big_dec_num.isZero())
        {
            this._decomposition = [0];
            this._big_base10Value = BigInteger.ZERO;
        }
        else {
            this._big_base10Value = big_dec_num;
            let i = 0;
            let temp_divRem;
            while (!big_dec_num.isZero() && i < this._decomposition.length) {
                temp_divRem = big_dec_num.divRem(this._radix);
                big_dec_num = temp_divRem[0];
                this._decomposition[i] = temp_divRem[1].toJSValue();
                i++;
            }

            if (big_dec_num.isZero()) {
                while (i < this._decomposition.length) {
                    this._decomposition[i] = 0;
                    i++;
                }
            }
            else {
                while (!big_dec_num.isZero()) {
                    temp_divRem = big_dec_num.divRem(this._radix);
                    big_dec_num = temp_divRem[0];
                    this._decomposition.push(temp_divRem[1].toJSValue());
                }
            }
        }
    }
}

class Base10DigitConverter
{
    /**
     * @param {number} radix The radix for converting. An integer >= 1
     * @param {function(number) => string} digitToString Callback to transform a digit of the base in a string representation. It's reference will be stored in this.callbackDigitToString
     * digitToString parameter is assumed to return a different non empty string for every value passed
     * @param {boolean} caseInsensitive Tell if string for digit representations should have the same values if they are uppercase or downcase. Default is true
     */
    constructor(radix, digitToString, caseInsensitive = true)
    {
        throwIfNotValidRadix(radix, 1);

        this._radix = radix;

        testDigitToStringCallback(digitToString, 0, radix - 1, 'digitToString');

        this._callbackDigitToString = digitToString;

        throwIfNotExpectedTypeOf(caseInsensitive, typeof true, 'caseInsensitive', true);

        this._isCaseInsensitive = caseInsensitive;

        //this._valuesArray = Array(radix).fill(undefined).map((value, index) => ''.concat(digitToString(index)));
        /**
         * @type {string[]}
         */
        this._valuesArray = [];
        
        for(let i=0; i < radix; i++)
        {
            let val = this._prepareRadixDigit( digitToString(i) );

            let indexOf = this._valuesArray.indexOf(val);
            if(indexOf >= 0)
                throw new Error("digitToString callback must return different values for every integer value passed in the range 0, " + (radix-1) + ". Found digitToString(" + i + ") = " + val + " is the same as digitToString(" + indexOf + ")");

            this._valuesArray.push(val);
        }
    }

    get radix()
    {
        return this._radix;
    }

    get callbackDigitToString()
    {
        return this._callbackDigitToString;
    }

    get isCaseInsensitive()
    {
        return this._isCaseInsensitive;
    }

    /**
     * Return the string representing the base 10 value passed. Must be a valid value for the digit, so it must be an integer between 0 and this.radix - 1
     * @param {number} base10value  0 <= base10value < this.radix
     * @returns {string} The uppercased version of the digit if it's case insensitive
     */
    getConvertedDigit(base10value)
    {
        if(!Number.isInteger(base10value) || base10value < 0 || base10value >= this._radix)
            throw new Error("base10value must be converted to a digit in base " + this._radix + ", so must be a value between 0 and " + (this._radix - 1) + ". Found + '" + base10value + "'");

        return this._valuesArray[base10value];
    }

    _prepareRadixDigit(radixDigit)
    {
        if(this._isCaseInsensitive)
            return ''.concat(radixDigit).toUpperCase();
        else
            return ''.concat(radixDigit);
    }

    /**
     * Return the integer base 10 value of a string representing a digit
     * @param {string} radixDigit A valid digit for the radix used. If the converter is case insensitive, will be used the uppercased version
     * @returns {number} A number between 0 and this.radix - 1. Because this.radix it's a safe integer, either the result will be a safe integer
     */
    getBase10Value(radixDigit)
    {
        let str_val = this._prepareRadixDigit(radixDigit);

        let res = this._valuesArray.indexOf(str_val);

        if(res < 0)
            throw new Error("radixDigit must be a digit used for base " + this._radix + ". One of [" + this._valuesArray + "]. Found " + str_val + "(original: " + radixDigit + ")");

        return res;
    }

    /**
     * Return if the string passed is a valid digit representation for the radix used
     * @param {string} radixDigit The string to test. If the converter is case insensitive, will be used the uppercased version
     * @returns {boolean}
     */
    isAValidRadixDigit(radixDigit)
    {
        return this._valuesArray.includes( this._prepareRadixDigit(radixDigit) );
    }
}

class Base10Converter{


    /**
     * @param {number} radix The radix for converting. An integer >= 2
     * @param {function(number) => string} digitToString Callback to transform a digit of the base in a string representation. It's reference will be stored in this.callbackDigitToString
     * @param {boolean} caseInsensitive Tell if string for number representations should have the same values if they are uppercased or downcased. Default is true
     */
    constructor(radix, digitToString, caseInsensitive = true)
    {
        throwIfNotValidRadix(radix, 2);

        this._base10digitConverter = new Base10DigitConverter(radix, digitToString, caseInsensitive);
        this._big_radix = BigInteger(radix);

        this._multipliers = Array(7);
        this._multipliers[0] = BigInteger.ONE;
        for(let i=1; i < this._multipliers.length; i++)
            this._multipliers[i] = this._multipliers[ i-1 ].multiply(this._big_radix);
    }

    get digitConverter()
    {
        return this._base10digitConverter;
    }

    /**
     * @param {number[]} base10Decomposition A decomposition of the number in the base used, with integer values (>=0) of the digits
     * @returns {string[]} An array of strings of the same length of base10Decomposition. At the index i of the result will be the converted digit at index i in base10Decomposition
     * @throws {Error} If there is a digit not in the range [0, this.digitConverter.radix - 1]
     */
    getRadixDecomposition(base10Decomposition)
    {
        return base10Decomposition.map((value) => this._base10digitConverter.getConvertedDigit(value), this);
    }

    /**
     * @param {string[]} radixDecomposition A decomposition of digit representations' of a number in the base used
     * @returns {number[]} An array of integers of the same length of radixDecomposition. At the index i of the result will be the value of the digit at index i in radixDecomposition. 
     *                     Every value will be in the range [0, this.digitConverter.radix - 1]
     * @throws {Error} If there is an inconvertible digit
     */
    getBase10Decomposition(radixDecomposition)
    {
        return radixDecomposition.map((value) => this._base10digitConverter.getBase10Value(value), this);        
    }

    /**
     * Return a string representing the number passed as decomposition
     * @param {number[]} base10Decomposition A decomposition of the number in the base used, with integer values (>=0) of the digits
     * @returns {string}
     * @throws {Error} If there is a digit not in the range [0, this.digitConverter.radix - 1]
     */
    getConvertedString(base10Decomposition)
    {
        return this.getRadixDecomposition(base10Decomposition).reduceRight((a, b) => a.concat(b));
    }

    /**
     * Return a number representing the base 10 value of the decomposition passed
     * @param {string[]} radixDecomposition A decomposition of digit representations' of a number in the base used
     * @returns {BigInteger} A BigInteger object representing the number converted from the representation
     */
    getBase10Value(radixDecomposition) {
        let result = BigInteger.ZERO;
        let base10decomposition = this.getBase10Decomposition(radixDecomposition);
        
        //Se i moltiplicatori per la conversione sono minori degli elementi da convertire, calcolo i moltiplicatori mancanti
        if(this._multipliers.length < base10decomposition.length)
        {
            let j = this._multipliers.length;
            //Creo un array per i moltiplicatori mancanti
            this._multipliers = this._multipliers.concat( Array(base10decomposition.length - this._multipliers.length) );

            let prev_multiplier = this._multipliers[ j - 1 ];
            while(j < this._multipliers.length)
            {
                prev_multiplier = prev_multiplier.multiply(this._big_radix);
                this._multipliers[j] = prev_multiplier;
                
                j++;
            }
        }

        for (let i=0; i < base10decomposition.length; i++) {
            result = result.add(this._multipliers[i].multiply( base10decomposition[i] ));
        }

        return result;
    }

    /**
     * Return if the array passed is a valid decomposition of the digits representation for the radix used
     * @param {string[]} radixDecomposition
     * @returns {boolean}
     */
    isAValidRadixDecomposition(radixDecomposition)
    {
        return radixDecomposition.every( (value) => this._base10digitConverter.isAValidRadixDigit(value) , this);
    }


}